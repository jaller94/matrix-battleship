# Battleships via Matrix

This specification aims to allow two players to play a game of Battleship via the [Matrix](https://spec.matrix.org) protocol. It assumes that there's zero trust between the players.

The version of this specification is `1.0`. **This is a draft, not a release!**

This draft has the following issues:
- [ ] For the lack of a system, the challenged player goes first.
- [ ] Undecided whether to reference a game by a `gameId` or its first Matrix message.

## Game and Rules

The game is played on two 10x10 grid boards, one belonging to each player.

First, both players place all their ships on their board. The ship placement is kept secret from the other player. This is the setup phase.

Then, in the battle phase, the players take turns announcing shots at the other player's board. One shot hits an individual cell on the opponent's board. A shot is confirmed to be a miss or a hit, including the information which ship has been hit.

A ship is sunk when all cells it occupies have been hit. Once a player has hit all their opponent's ships, they should declare victory.

### Ships
The following ships must be placed by each player:
* 1x `carrier` (length 5)
* 1x `battleship` (length 4)
* 1x `destroyer` (length 3)
* 1x `submarine` (length 3)
* 1x `patrol-boat` (length 2)

## Setup phase
A game gets started by one player challenging another and the challenge getting accepted.

```websequencediagrams
title Battleships Initialization (Success)

note over Alice: Places her ships
Alice->Bob: Challenge
note over Bob: Places his ships
Bob->Alice: Accepts
```

The initial challenge includes this specification's version, an ID for the game, one or more recipients of the challenge and a hash of the board.

The game ID (`id`) MUST BE a string between 16 and 255 bytes. When Alice sends a challenge her client should generate a random string. It is in Alice's interest to make the game ID as long as possible to keep the game ID unique for her and any recipient and prevent Bob from brute-forcing a different board where he may win the game.

This specification's version (`version`) SHOULD BE included for Bob to determine the compatibility with his client. Bob SHOULD NOT accept different major versions (e.g. `2.0`) but MAY accept challenges with the same minor version (e.g. `1.1`). Expect the version format to match the regular expression `/^\d+\..+/`.

Alice MAY include a list of recipients for her challenge (`recipients`). It MUST BE stored as an array of strings. Every string SHOULD match a Matrix homeserver address or a specific user. If the list of recipients is missing, any recipient of the Matrix message may accept her challenge. An empty list is invalid - maybe she wishes to play by herself.

The challenge MUST include a `boardHash`. Using the `boardHash`, Alice can prove that she didn't cheat once the game is over. Until the game is over she only sends the board's hash (`boardHash`). To create the board hash, stringify your board to JSON and hash it. The hash MUST be represented as a [multihash](https://multiformats.io/multihash/) and SHOULD be using the hash function `sha2-256` in a hex representation. If the sender varies from this format they risk that no opponent supports their challenge.

Example of a challenge:
```javascript
{
  "version": "1",
  "gameId": "d51c28b3-2583-4c02-abde-9ee6e5c07310",
  "boardHash": "122041dd7b6443542e75701aa98a0c235951a28a0d851b11564d20022ab11d2589a8",
  "recipients": [
    "@alice:example.com",
    "matrix.org"
  ]
}
```

### Board

The board contains the game ID, Alice's ship placement and a secret string. The game ID (`id`) will prove to Bob that she placed the ships for this game and prevents her from cheating. If she rearranged ships, her `boardHash` would change. The ship placement (`ships`) declares where Alice hid her ships. The secret string (`secret`) prevents Bob from brute-forcing Alice's ship placement. Bob is aware of how this hash gets constructed, so a secret must be included to salt the hash. Once Alice's client thinks that she has won the game, it reveals her board, allowing Bob to verify her ship placement and hash.

The property `ships` is an array of Ship objects. A ship object MUST include a `length` of the ship, zero-based coordinates (`position`) and the direction a ship is `heading`. The length MUST be a positive integer.

Example of a board:
```javascript
{
  "gameId": "d51c28b3-2583-4c02-abde-9ee6e5c07310",
  "ships": [
    {
      "heading": "north"
      "position": [1,1],
      "type": "patrol-boat",
    },
    {
      "heading": "west"
      "position": [5,8],
      "type": "submarine",
    },
    ...
  ],
  "secret": "b3385587-d751-415f-8547-94d4c86579a6"
}
```

#### Validation of a board

To verify the opponent didn't cheat, the loser may want to verify the board of their opponent.

1. The property `gameId` MUST match the game id sent in the challenge.
2. The property `ships` MUST be an array of ship objects.
  1. Each ship MUST have a property `type` that's one of the five strings: `carrier`, `battleship`, `destroyer`, `submarine`, `patrol-boat`.
  2. Each ship MUST have a property `position` containing two integers, which each satisfy: `0 =< x < 10`.
  3. Each ship MUST have a property `heading` that's one of the four strings: `north`, `east`, `south`, `west`.
3. Validate ship placement
  1. [All ships](#Ships) MUST be placed and there MUST NOT be any additional ships.
  2. Ships MUST NOT overlap any other ships. Remember that each type has an associated length and occupies more than one cell.
  3. Ships MUST fit within the 10x10 grid. Remember that all coordinates are zero-based.

### Accept a challenge

Any recipient may accept an unanswered challenge. The player MUST assume the game to start immediately. The opponents client will send a game error, if there is an issue.

If Bob hasn't instructed its client to blindly trust Alice, it SHOULD verify that Alice's `boardHash` is supported. For the hash function `sha2-256` in hex representation `boardHash` matches the following regular expression `/^1220[\da-f]{64}$/i`.

Example of accepting a challenge:
```javascript
{
  "gameId": "d51c28b3-2583-4c02-abde-9ee6e5c07310",
  "boardHash": "122041dd7b6443542e75701aa98a0c235951a28a0d851b11564d20022ab11d2589a8"
}
```

### Decline a challenge

Codes to decline a challenge:

- `INVALID_MESSAGE` - A required property is missing, has an invalid type, exceeds the expected range, etc.
- `UNSUPPORTED_VERSION` - The battleship version is not supported by the client.
- `UNKNOWN_HASH_FORMAT` - The hashing function is not supported.
- `GAME_ID_IN_USE` - The client already knows an active game with that ID.
- `NOT_INTERESTED` - The user declined the challenge.

Example of declining a challenge:
```javascript
{
  "gameId": "d51c28b3-2583-4c02-abde-9ee6e5c07310",
  "declineReason": "UNKNOWN_HASH"
}
```

## Battle phase

Now the players start to take turns. The player who accepted the challenge begins.

```websequencediagrams
title Battleships Battle phase

actor Alice
Alice->Alice's client: Decides where to shoot
Alice's client->Bob's client: Sends shot
actor Bob
Bob's client->Alice's client: Reports a hit or miss
Bob's client->Bob: Updates Bob's perspective
Bob->Bob's client: Decides where to shoot
Bob's client->Alice's client: Sends shot
Alice's client->Bob's client: Reports a hit or miss
Alice's client->Alice: Updates Alice's perspective
```

### Fire a shot

Example of a shot:

```javascript
{
  "gameId": "d51c28b3-2583-4c02-abde-9ee6e5c07310",
  "position": [4,3]
}
```

### Declare a hit / miss

Each shot MUST be followed by a shot response. By sending a shot response a player starts their turn.

A shot response MUST include: `gameId` and `position`. In case a ship has been hit, it MUST also include `hit` where the value matches on of the ship types.

Example of a hit:

```javascript
{
  "gameId": "d51c28b3-2583-4c02-abde-9ee6e5c07310",
  "position": [4,3],
  "hit": "destroyer"
}
```

Example of a miss:

```javascript
{
  "gameId": "d51c28b3-2583-4c02-abde-9ee6e5c07310",
  "position": [5,5]
}
```

### Reject move

_In consideration_

The following messages can be sent to point out mistakes without declaring the game over. Both clients MUST ignore the referred shot or, if this is unacceptable, end the game with `INVALID_STATE`.

- `NOT_YOUR_TURN` - The opponent's shot wasn't accepted. It's not their turn.
- `DUPLICATE_SHOT` - The opponent shot at a position for the second time. This gives them another chance.

## Game over

The game may end with one player declaring victory or defeat, or with a game error.

```websequencediagrams
title Battleships: Declaring a winner

actor Alice
note over Alice's client: Finds Alice's last ship to be sunk
Alice's client->Bob's client: Declares Bob the winner
actor Bob
Alice's client->Alice: Displays defeat
Bob's client->Bob: Displays victory
```

### Declaring a winner

Check for the following conditions to declare a winner.

1. [The board MUST be valid](#validation-of-a-board).
2. All cells occupied by ships MUST have been hit with a shot by the opponent.

A declaration of a winner MAY be sent by either party. It MUST include the `gameId`, a `winner` and their `board`. The winner MUST be the string `you` or `me`. The board MUST be the initial JSON representation of the board used to create `boardHash`.

Example of declaring a winner:
```javascript
{
  "gameId": "d51c28b3-2583-4c02-abde-9ee6e5c07310",
  "winner": "you",
  "board": "{\"gameId\":\"d51c28b3-2583-4c02-abde-9ee6e5c07310\",\"ships\":[{..."
}
```

In most cases the loser knows about their loss first and SHOULD declare the other player the winner. If there's no such message, the winner SHOULD declare their own victory after getting a confirmation for their last required hit.

The other client should respond with a declaration of a winner and their `board`. Each client MAY verify the opponent's board against the inital `boardHash`. If it doesn't match, they SHOULD send an `INVALID_STATE` error.

### Validation of fair play

In addition to the validation done for declaring a winner, a client MAY check that the opponent played fairly.

1. Each hit response must have been correct, including the information of which ship was hit.
2. Each miss response must have been correct.

### Game errors

Either client may end the game by declaring an error.

Error types:

- `INVALID_MESSAGE` - A required property is missing, has an invalid type, exceeds the expected range, etc.
- `INVALID_STATE` - The opponents board is not valid or they incorrectly declared a winner. The sending client MAY count this as a victory for their player. This may also be send when the opponent (repeatedly) sends a shot when it's not their turn.
- `SURRENDER` - The player chose to surrender. The receiving client MUST count this a victory for their own player.
- `UNKNOWN_GAME_ID` - The client is not familiar with a game of that ID with the other Matrix account.

Under consideration:
- `CHEATING` - Like `INVALID_STATE` but calling out the other player/client for cheating.
- `LEFT` - Like `SURRENDER` but without the admission of a defeat. The receiving client MUST NOT count this as a victory for their player without interaction. A client MAY ask the player if they want to declare themselves the winner for their internal statistic.
