import {
    WIDTH,
    HEIGHT,
    SHIPS,
} from './globals.js';

class WCBoard extends HTMLElement {
    static get template() {
        return `
            <style>
                table {
                    user-select: none;
                    width: 21em;
                    position: relative;
                }

                td {
                    width: 1.5em;
                    height: 1.5em;
                    border: 1px solid black;
                    background-color: #3A506B;
                }

                td.ship::after {
                    display: block;
                    content: '';
                    width: 1.3em;
                    height: 1.3em;
                    margin: 0.1em;
                    background-color: yellow;
                    border-radius: 10%;
                }

                td.fired::after {
                    display: block;
                    content: '';
                    width: 1em;
                    height: 1em;
                    margin: 0.25em;
                    background-color: white;
                    border-radius: 50%;
                }

                td.fired.hit::after {
                    background-color: red;
                }
                
                table.interactive td:not(.fired):hover::after {
                    display: block;
                    content: '';
                    width: 1em;
                    height: 1em;
                    margin: 0.25em;
                    background-color: rgba(255,255,255, 0.5);
                    border-radius: 50%;
                }

                table.interactive.place-west td:not(.fired):hover::after {
                    transform: rotate(-90deg);
                    transform-origin: 0 0;
                    margin: .5em;
                }

                table.interactive.place-south td:not(.fired):hover::after {
                    transform: rotate(-180deg);
                    transform-origin: 0 0;
                    margin: 0.25em 1.25em;
                }

                table.interactive.place-east td:not(.fired):hover::after {
                    transform: rotate(-270deg);
                    transform-origin: 0 0;
                    margin: -0.5em 1em;
                }

                table.interactive.place-carrier td:hover::after {
                    display: block;
                    position: absolute;
                    margin-top: 0;
                    height: 8em;
                    background-color: rgba(255,255,0, 0.5);
                    border-radius: 10%;
                }

                table.interactive.place-battleship td:hover::after {
                    display: block;
                    position: absolute;
                    margin-top: 0;
                    height: 6em;
                    background-color: rgba(255,255,0, 0.5);
                    border-radius: 10%;
                }

                table.interactive.place-destroyer td:hover::after {
                    display: block;
                    position: absolute;
                    margin-top: 0;
                    height: 4em;
                    background-color: rgba(255,255,0, 0.5);
                    border-radius: 10%;
                }

                table.interactive.place-submarine td:hover::after {
                    display: block;
                    position: absolute;
                    margin-top: 0;
                    height: 4em;
                    background-color: rgba(255,255,0, 0.5);
                    border-radius: 10%;
                }

                table.interactive.place-patrol-boat td:hover::after {
                    display: block;
                    position: absolute;
                    margin-top: 0;
                    height: 2em;
                    background-color: rgba(255,255,0, 0.5);
                    border-radius: 10%;
                }
            </style>
            <table></table>
        `;
    }

    static get cache() {
        this._cache = this._cache || {};
        return this._cache;
    }

    constructor() {
        super(); // always call super() first in the constructor

        let template = this.constructor.cache[this.constructor.name];
        if (!template) {
            template = document.createElement('template');
            template.innerHTML = this.constructor.template;
            this.constructor.cache[this.constructor.name] = template;
        }

        let node = template.content.cloneNode(true);
        this.root = this.attachShadow({ mode: 'open' });
        this.root.appendChild(node);

        const width = WIDTH;
        const height = HEIGHT;

        const table = this.root.querySelector('table');
        const row = document.createElement('tr');
        table.appendChild(row);
        const cell = document.createElement('tr');
        row.append(cell);
        for (let x = 0; x < width; x++) {
            const cell = document.createElement('th');
            cell.setAttribute('scope', 'column');
            cell.innerText = x + 1;
            row.append(cell);
        }
        for (let y = 0; y < height; y++) {
            const row = document.createElement('tr');
            const rowIndex = document.createElement('th');
            rowIndex.setAttribute('scope', 'row');
            rowIndex.innerText = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[y];
            row.append(rowIndex);
            for (let x = 0; x < width; x++) {
                const cell = document.createElement('td');
                cell.setAttribute('x', x);
                cell.setAttribute('y', y);
                row.appendChild(cell);
            }
            table.appendChild(row);
        }
        table.addEventListener('click', this.onClickGrid.bind(this));

        const callback = (mutationsList) => {
            console.debug(mutationsList);
            for (const mutation of mutationsList) {
                if (mutation.type === 'childList') {
                    console.debug(mutation);
                }
            }
        };

        const observer = new MutationObserver(callback);
        observer.observe(this.root, {
            childList: true,
            subtree: true,
        });

        // This is a hack!
        setInterval(() => {
            for (const node of this.children) {
                if (node.getAttribute('type')) {
                    this.placeShip(node);
                } else {
                    this.placeShot(node.getAttribute('x'), node.getAttribute('y'), node.getAttribute('hit') === 'true')
                }
            }
        }, 500);
    }

    static get observedAttributes() {
        return ['interactive', 'place-ship', 'place-direction'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        const table = this.root.querySelector('table');
        if (name === 'interactive') {
            this.interactive = newValue === 'true';
            if (this.interactive) {
                table.classList.add('interactive');
            } else {
                table.classList.remove('interactive');
            }
        }
        if (name === 'place-ship') {
            if (oldValue) {
                table.classList.remove(`place-${oldValue}`);
            }
            if (newValue) {
                table.classList.add(`place-${newValue}`);
            }
        }
        if (name === 'place-direction') {
            if (oldValue) {
                table.classList.remove(`place-${oldValue}`);
            }
            if (newValue) {
                table.classList.add(`place-${newValue}`);
            }
        }
    }

    onClickGrid(event) {
        const x = Number.parseInt(event.target.getAttribute('x'));
        const y = Number.parseInt(event.target.getAttribute('y'));
        if (!Number.isNaN(x) && !Number.isNaN(y)) {
            const checkEvent = new CustomEvent('target', {
                bubbles: true,
                cancelable: false,
                detail: {
                    x,
                    y,
                },
            });
            this.dispatchEvent(checkEvent);
        }
    }

    placeShip(ship) {
        const x = Number.parseInt(ship.getAttribute('x'));
        const y = Number.parseInt(ship.getAttribute('y'));
        const length = SHIPS[ship.getAttribute('type')];
        for (let i = 0; i < length; i++) {
            const direction = ship.getAttribute('direction');
            const xOffset = direction === 'west' ? 1 : direction === 'east' ? -1 : 0;
            const yOffset = direction === 'north' ? 1 : direction === 'south' ? -1 : 0;
            const cell = this.root.querySelector(`td[x="${x+xOffset*i}"][y="${y+yOffset*i}"]`);
            if (cell) {
                cell.classList.add('ship');
            }
        }
    }

    placeShot(x, y, hit = false) {
        const cell = this.root.querySelector(`td[x="${x}"][y="${y}"]`);
        if (cell) {
            cell.classList.add('fired');
            if (hit) {
                cell.classList.add('hit');
            }
        }
    }
}
window.customElements.define('wc-board', WCBoard);
