let widgetApi = null;
let userId = null;

function handleError(error) {
    console.error(error);
}

function parseFragment() {
    const fragmentString = (window.location.hash || "?");
    return new URLSearchParams(fragmentString.substring(Math.max(fragmentString.indexOf('?'), 0)));
}

function assertParam(fragment, name) {
    const val = fragment.get(name);
    if (!val) throw new Error(`${name} is not present in URL - cannot load widget`);
    return val;
}

window.postState = ({gameId, state, ourBoard, theirBoard}) => {
    widgetApi.sendRoomEvent('de.chrpaul.games.battleship', {
        gameId,
        ourBoard,
        state,
        theirBoard,
    });
}

try {
    const qs = parseFragment();
    const widgetId = assertParam(qs, 'widgetId');
    userId = assertParam(qs, 'userId');
    console.error(widgetId, userId);

    // Set up the widget API as soon as possible to avoid problems with the client
    widgetApi = new mxwidgets.WidgetApi(widgetId);
    // widgetApi.requestCapability(mxwidgets.MatrixCapabilities.AlwaysOnScreen);

    // We want to be able to send game state
    widgetApi.requestCapabilityToSendEvent('de.chrpaul.games.battleship');

    // ... and receive it
    widgetApi.requestCapabilityToReceiveEvent('de.chrpaul.games.battleship');

    widgetApi.on('ready', async() => {
        try {
            const events = await widgetApi.readRoomEvents('de.chrpaul.games.battleship');
            const latestEvent = events.pop();
            window.receiveState(latestEvent.content);
        } catch (error) {
            console.error('Failed to get past state events', error);
        }
    });

    widgetApi.on('action:send_event', ev => {
        ev.preventDefault();
        widgetApi.transport.reply(ev.detail, {}); // ack

        const mxEvent = ev.detail.data;
        if (mxEvent.type === 'de.chrpaul.games.battleship') {
            if (mxEvent.sender !== userId) {
                window.receiveState(mxEvent.content);
            }
        }
    });

    // Start the widget as soon as possible too, otherwise the client might time us out.
    widgetApi.start();
    // If waitForIframeLoad is false, tell the client that we're good to go
    // widgetApi.sendContentLoaded();
} catch (e) {
    handleError(e);
}
