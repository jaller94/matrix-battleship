import { html, useCallback, useState } from '../node_modules/htm/preact/standalone.module.js';
import Game from './game.js';

export default function App() {
    const [gameId, setGameId] = useState('aaa');

    const sendState = useCallback((gameState) => {
        if (!window.postState) {
            return;
        }
        let state = '';
        if (gameState.status === Status.waitToBegin) {
            state = 'challenge';
        }
        window.postState({
            gameId,
            ourBoard: gameState.ourBoard,
            state,
            theirBoard: gameState.theirBoard,
        });
    }, [gameId]);

    if (gameId) {
        return html`<${Game} onSendState=${sendState} />`;
    }

    return html`
        <div></div>
    `;
}