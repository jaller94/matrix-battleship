export const WIDTH = 10;
export const HEIGHT = 10;

export const SHIPS = {
    carrier: 5,
    battleship: 4,
    destroyer: 3,
    submarine: 3,
    'patrol-boat': 2,
};
