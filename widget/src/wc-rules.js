class WCRules extends HTMLElement {
    static get template() {
        return `
            <style>
                table {
                    user-select: none;
                    width: 21em;
                }
            </style>
            <h2>Rules</h2>
            <p>In the setup phase both players place five ships on their board. Each player has a carrier (length 5), a battleship (length 4), a destroyer (length 3), a submarine (length 3), and a patrol boat (length 2).</p>
            <p>In the battle phase the players take turns shooting at each other's board. All cells any ship occupies have to be hit for the opponent to win.</p>
        `;
    }

    static get cache() {
        this._cache = this._cache || {};
        return this._cache;
    }

    constructor() {
        super(); // always call super() first in the constructor

        let template = this.constructor.cache[this.constructor.name];
        if (!template) {
            template = document.createElement('template');
            template.innerHTML = this.constructor.template;
            this.constructor.cache[this.constructor.name] = template;
        }

        let node = template.content.cloneNode(true);
        this.root = this.attachShadow({ mode: 'open' });
        this.root.appendChild(node);
    }
}
window.customElements.define('wc-rules', WCRules);
