import { html, render, useCallback, useMemo, useRef, useState } from '../node_modules/htm/preact/standalone.module.js';
import L from '../locales/en.js';
import {
    randomInt,
    randomItem,
    randomString,
} from './helpers.js';
import {
    WIDTH,
    HEIGHT,
    SHIPS,
} from './globals.js';

/**
 * The states a local game can be in.
 * @readonly
 * @enum {number}
 */
const Status = {
    placeShip: 1,
    waitToBegin: 2,
    ourTurn: 3,
    theirTurn: 4,
    gameOver: 5,
};


/**
 * @typedef {string} ShipType
 * @typedef {'north'|'east'|'south'|'west'} Direction
 * @typedef {[number, number, (ShipType|undefined)]} Shot
 * 
 * @typedef {Object} Ship
 * @property {ShipType} type
 * @property {number} x
 * @property {number} y
 * @property {Direction} direction
 * 
 * @typedef {Object} Board
 * @property {Ship[]} ships
 * @property {Shot[]} shots
 * 
 * @typedef {Object} GameState
 * @property {Board} ourBoard
 * @property {Board} theirBoard
 * @property {Status} status
 * @property {Direction} placingDirection
 * @property {string} gameId
 * @property {ShipType[]} shipsToPlace
 */


/**
 * Returns if all ships on a board have been sunk.
 * A ship sinks when all cells it occupies have been hit.
 * @returns {boolean}
 */
function areAllShipsSunk(board) {
    const collisionArray = getCollisionArray(board.ships, board.shots);
    return !collisionArray.includes(true);
}

/**
 * Returns whether a shot is a hit.
 * @returns {boolean}
 */
function isHit(board, x, y) {
    const collisionArray = getCollisionArray(board.ships);
    const index = x + y * WIDTH;
    return collisionArray[index] !== undefined;
}

/**
 * Returns if a position has already been shot at.
 * @param {Shot[]} shots
 * @param {number} x
 * @param {number} y
 * @returns {boolean}
 */
const hasAlreadyBeenShotAt = (shots, x, y) => {
    return shots.some((shot) => (x === shot[0] && y === shot[1]));
};

/**
 * Returns which ship has been hit at a position.
 * This returns undefined, if the position has not been shot at. 
 * @returns {ShipType|undefined}
 */
function whichShipHit(board, x, y) {
    const collisionArray = getCollisionArray(board.ships, undefined, undefined, true);
    const index = x + y * WIDTH;
    return collisionArray[index];
}

/**
 * Returns an 2D array describing where ships are on the grid.
 * @returns {(undefined|boolean|ShipType)[]}
 */
function getCollisionArray(ships = [], shots = [], array, returnShipType=false) {
    array = array || Array(WIDTH * HEIGHT);
    for (const ship of ships) {
        const length = SHIPS[ship.type];
        const direction = ship.direction;
        const xOffset = direction === 'west' ? 1 : direction === 'east' ? -1 : 0;
        const yOffset = direction === 'north' ? 1 : direction === 'south' ? -1 : 0;
        for (let i = 0; i < length; i++) {
            const index = ship.x + xOffset * i + (ship.y + yOffset * i) * WIDTH;
            array[index] = returnShipType ? ship.type : true;
        }
    }
    for (const shot of shots) {
        const index = shot[0] + shot[1] * WIDTH;
        array[index] = false;
    }
    return array;
}

/**
 * Determines if a ship can be placed on the board. Checks if it
 * is within the board and if it collides with other ships.
 * @param {Board} board
 * @param {Ship} ship
 * @returns {boolean} true, if the ship can be added.
 */
function isShipValid(board, ship) {
    const length = SHIPS[ship.type];
    const x = ship.x;
    const y = ship.y;
    const direction = ship.direction;
    const xOffset = direction === 'west' ? 1 : direction === 'east' ? -1 : 0;
    const yOffset = direction === 'north' ? 1 : direction === 'south' ? -1 : 0;
    const minX = Math.min(x, x + xOffset * (length - 1));
    const minY = Math.min(y, y + yOffset * (length - 1));
    const maxX = Math.max(x, x + xOffset * (length - 1));
    const maxY = Math.max(y, y + yOffset * (length - 1));
    if (minX < 0 || minY < 0 || maxX > WIDTH - 1 || maxY > HEIGHT - 1) {
        // There ship is placed outside the grid.
        return false;
    }
    const collisionArray = getCollisionArray(board.ships);
    for (let i = 0; i < length; i++) {
        const index = x + xOffset * i + (y + yOffset * i) * WIDTH;
        if (collisionArray[index] === true) {
            // There is already a ship here.
            return false;
        }
    }
    return true;
}

/**
 * Returns a position where a bot would place a ship.
 * This function uses randomness and is non-deterministic.
 * @param {Board} board
 * @param {ShipType} shipType The type of a ship which shall be placed
 * @returns {Ship}
 */
function botPlaceShip(board, shipType) {
    let ship;
    do {
        ship = {
            type: shipType,
            direction: randomItem('north,west,east,south'.split(',')),
            x: randomInt(0, WIDTH - 1),
            y: randomInt(0, HEIGHT - 1),
        };
    } while (!isShipValid(board, ship));
    return ship;
}

/**
 * Returns a position where a bot would place a shot.
 * This function uses randomness and is non-deterministic.
 * @param {Board} board
 * @returns {[number, number]}
 */
function botPlaceShot(board) {
    let x;
    let y;
    const preferCheckerboard = board.shots.length < ((WIDTH * HEIGHT / 2) - 3);
    const isCheckerboard = (x, y) => {
        return y % 2 === 0 ? x % 2 === 1 : x % 2 === 0;
    }
    const isHit = (x, y) => {
        const shot = board.shots.find((shot) => (x === shot[0] && y === shot[1]));
        if (shot) {
            return !!shot[2];
        }
        return false;
    }
    const isNeighborHit = (x, y) => {
        return isHit(x - 1, y) || isHit(x + 1, y) || isHit(x, y - 1) || isHit(x, y + 1);
    }
    let attempt = 0;
    do {
        x = randomInt(0, WIDTH - 1);
        y = randomInt(0, HEIGHT - 1);
        // When the checkerboard is done but we shot plenty of ships already we might not have completed the checkerboard
        attempt++;
        if (attempt > 100 && !hasAlreadyBeenShotAt(board.shots, x, y) && isCheckerboard(x, y)) {
            break;
        }
    } while (
        // Retry if we already shot there
        hasAlreadyBeenShotAt(board.shots, x, y) ||
        // Retry if we prefer the checkerboard and it's neither a checkerboard place nor a neighbor of a successful hit
        (preferCheckerboard && (!isCheckerboard(x, y) && !isNeighborHit(x, y))) ||
        // Retry if we don't prefer the checkerboard but it's not a neighbor of a hit
        (!preferCheckerboard && !isNeighborHit(x, y))
    );
    return [x, y];
}


export default function Game({ gameId }) {
    const ourGrid = useRef(null);
    const theirGrid = useRef(null);
    const [rotatingAllowed, setRotatingAllowed] = useState(true);
    const [instruction, instruct] = useState();
    const [gameState, setGameState] = useState({
        shipsToPlace: [],
        placingDirection: 'north',
        status: null,
        ourBoard: {
            name: null,
            ships: [],
            shots: [],
        },
        theirBoard: {
            name: null,
            ships: [],
            shots: []
        },
    });

    console.dir('gameState', gameState);

    const prepareToPlaceShip = useCallback((shipsToPlace) => {
        instruct(`Click a field to place your ${shipsToPlace[0]}.`);
    }, []);

    const handlePlaceShip = useCallback((event) => {
        const { x, y } = event.detail;
        const ship = {
            type: gameState.shipsToPlace[0],
            direction: gameState.placingDirection,
            x,
            y,
        };
        if (!isShipValid(gameState.ourBoard, ship)) {
            return;
        }
        setGameState(gameState => ({
            ...gameState,
            shipsToPlace: gameState.shipsToPlace.slice(1),
            ourBoard: {
                ...gameState.ourBoard,
                ships: [
                    ...gameState.ourBoard.ships,
                    ship,
                ]
            },
        }));
        if (gameState.shipsToPlace.length === 1) {
            setStatus(Status.waitToBegin);
        } else {
            prepareToPlaceShip(gameState.shipsToPlace.slice(1));
        }
    }, [gameState]);

    const handlePlaceShot = useCallback((event) => {
        const { x, y } = event.detail;
        if (hasAlreadyBeenShotAt(gameState.theirBoard.shots, x, y)) {
            return;
        }
        setGameState(gameState => ({
            ...gameState,
            theirBoard: {
                ...gameState.theirBoard,
                shots: [
                    ...gameState.theirBoard.shots,
                    [x, y, whichShipHit(gameState.theirBoard, x, y)],
                ]
            },
        }));
        // sendState();
        setStatus(Status.theirTurn);
    }, [gameState]);

    /**
     * @param {number|Direction} rotationsOrDirection
     */
    const handleRotateShip = useCallback((rotationsOrDirection = 1) => {
        /** @type {Direction} */
        let direction;
        if (typeof rotationsOrDirection === 'string') {
            direction = rotationsOrDirection;
        } else {
            /** @type {Direction[]} */
            const directions = ['north', 'east', 'south', 'west'];
            direction = gameState.placingDirection;
            direction = directions[(directions.indexOf(direction) + rotationsOrDirection + 1024) % 4];
        }
        setGameState(gameState => ({
            ...gameState,
            placingDirection: direction,
        }));
    }, [gameState]);

    /**
     * @param {Status} newStatus
     */
    const setStatus = useCallback((newStatus) => {
        if (newStatus === Status.placeShip) {
            setGameState(gameState => ({
                ...gameState,
                shipsToPlace: Object.keys(SHIPS),
            }));
            prepareToPlaceShip(Object.keys(SHIPS));
        }
        if (newStatus === Status.waitToBegin) {
            // sendState();
            setRotatingAllowed(false);
            for (const shipType of Object.keys(SHIPS)) {
                const ship = botPlaceShip(gameState.theirBoard, shipType);
                setGameState(gameState => ({
                    ...gameState,
                    theirBoard: {
                        ...gameState.theirBoard,
                        ships: [
                            ...gameState.theirBoard.ships,
                            ship,
                        ],
                    },
                }));
            }
            setTimeout(() => setStatus(Status.ourTurn), 100);
        }
        if (newStatus === Status.ourTurn) {
            if (areAllShipsSunk(gameState.ourBoard)) {
                instruct(L['LOST']);
                setStatus(Status.gameOver);
                return;
            }
            instruct(L['CLICK_TO_SHOOT']);
        }
        if (newStatus === Status.theirTurn) {
            if (areAllShipsSunk(gameState.theirBoard)) {
                instruct(L['WON']);
                setStatus(Status.gameOver);
                return;
            }
            instruct(L['WAIT_FOR_OPPONENT']);

            const [x, y] = botPlaceShot(gameState.ourBoard);

            // Bot code
            setGameState(gameState => ({
                ...gameState,
                ourBoard: {
                    ...gameState.ourBoard,
                    shots: [
                        ...gameState.ourBoard.shots,
                        [x, y, whichShipHit(gameState.ourBoard, x, y)],
                    ],
                },
            }));
            // TODO Don't call this with an outdated state
            // sendState();
            setStatus(Status.ourTurn);
        }
        if (newStatus === Status.gameOver) {
        }
        setGameState(gameState => ({
            ...gameState,
            status: newStatus,
        }));
    }, [gameState]);


    if (gameState.status === null) {
        setGameState(gameState => ({
            ...gameState,
            status: Status.placeShip,
        }));
        setTimeout(() => setStatus(Status.placeShip), 0);
    }

    return html`
        ${instruction && html`<div>${instruction}</div>`}
        <div class="boards">
            <div>
                <h2>Your board</h2>
                <wc-board
                    place-direction=${gameState.placingDirection}
                    place-ship=${gameState.shipsToPlace.length ? gameState.shipsToPlace[0] : undefined}
                    interactive=${gameState.status === Status.placeShip ? 'true' : undefined}
                    ref=${ourGrid}
                    ontarget=${gameState.status === Status.placeShip ? handlePlaceShip : (gameState.status === Status.ourTurn ? handlePlaceShot : undefined)}
                >
                    ${gameState.ourBoard.ships.map(ship => html`
                        <div direction=${ship.direction} type=${ship.type} x=${ship.x} y=${ship.y}/>
                    `)}
                    ${gameState.ourBoard.shots.map(shot => html`
                        <div hit=${shot[2]} x=${shot[0]} y=${shot[1]}/>
                    `)}
                </wc-board>
                <button disabled=${rotatingAllowed} onClick=${() => handleRotateShip(-1)} aria-label="Rotate ship leftwards">↪️</button>
                <button disabled=${rotatingAllowed} onClick=${() => handleRotateShip(1)} aria-label="Rotate ship rightwards">↩️</button>
            </div>
            ${gameState.status !== Status.placeShip && html`
                <div>
                    <h2>Opponent's board</h2>
                    <wc-board
                        interactive=${gameState.status === Status.ourTurn ? 'true' : false}
                        ref=${theirGrid}
                    >
                        ${gameState.theirBoard.ships.map(ship => html`
                            <div direction=${ship.direction} type=${ship.type} x=${ship.x} y=${ship.y}/>
                        `)}
                        ${gameState.theirBoard.shots.map(shot => html`
                            <div hit=${shot[2]} x=${shot[0]} y=${shot[1]}/>
                        `)}
                    </wc-board>
                </div>
            `}
        </div>

        <wc-modal>
            <wc-rules></wc-rules>
        </wc-modal>
        <wc-modal>
            <wc-shortcuts></wc-shortcuts>
        </wc-modal>
    `;
}
