import {
    randomInt,
    randomItem,
} from './helpers.js';

describe('randomInt', () => {
    test('returns exact value', () => {
        expect(randomInt(0, 0)).toBe(0);
        expect(randomInt(1, 1)).toBe(1);
    });
});
