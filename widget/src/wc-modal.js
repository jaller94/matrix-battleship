class WCModal extends HTMLElement {
    static get template() {
        return `
            <style>
                div.modal {
                    border: 1px black solid;
                    border-radius: 15px;
                    padding: 15px;
                }
            </style>
            <div class="modal">
                <slot></slot>
            </div>
        `;
    }

    static get cache() {
        this._cache = this._cache || {};
        return this._cache;
    }

    constructor() {
        super(); // always call super() first in the constructor

        let template = this.constructor.cache[this.constructor.name];
        if (!template) {
            template = document.createElement('template');
            template.innerHTML = this.constructor.template;
            this.constructor.cache[this.constructor.name] = template;
        }

        let node = template.content.cloneNode(true);
        this.root = this.attachShadow({ mode: 'open' });
        this.root.appendChild(node);
    }
}
window.customElements.define('wc-modal', WCModal);
