export const randomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const randomItem = (array) => {
    return array[randomInt(0, array.length - 1)];
};

export const randomString = (length = 14) => {
    return Math.random().toString(16).substr(2, length);
};
