class WCShortcuts extends HTMLElement {
    static get template() {
        return `
            <style>
                ul {
                    list-style: none;
                    padding: 0;
                }

                .key {
                    display: inline-block;
                    border: 1px solid grey;
                    border-radius: 4px;
                    text-align: center;
                    min-width: 1.2em;
                    min-height: 1.2em;
                    padding: 0.2em .2em;
                    background: lightgrey;
                }
            </style>
            <h2>Controls</h2>
            <ul>
                <li><span class="key">Left click</span> Place ship and shot</li>
                <li><span class="key">R</span> Rotate ship clockwise</li>
            </ul>
        `;
    }

    static get cache() {
        this._cache = this._cache || {};
        return this._cache;
    }

    constructor() {
        super(); // always call super() first in the constructor

        let template = this.constructor.cache[this.constructor.name];
        if (!template) {
            template = document.createElement('template');
            template.innerHTML = this.constructor.template;
            this.constructor.cache[this.constructor.name] = template;
        }

        let node = template.content.cloneNode(true);
        this.root = this.attachShadow({ mode: 'open' });
        this.root.appendChild(node);
    }
}
window.customElements.define('wc-shortcuts', WCShortcuts);
