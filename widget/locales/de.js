export default {
    CLICK_TO_SHOOT: 'Klicke ein Feld zum Schließen. Rate, wo die Gegenseite ihre Schiffe versteckte.',
    LOST: 'All deine Schiffe wurden versenkt. Die Gegenseite gewinnt!',
    SHIP_CARRIER: 'Flugzeugträger',
    SHIP_BATTLESHIP: 'Schlachtschiff',
    SHIP_DESTROYER: 'Zerstörer',
    SHIP_SUBMARINE: 'U-Boot',
    SHIP_PATROL_BOAT: 'Patrouille-Boot',
    WAIT_FOR_OPPONENT: 'Warte bis %OPPONENT% den Zug beendet.',
    WON: 'Du hast gewonnen!',
};