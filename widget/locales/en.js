export default {
    CLICK_TO_SHOOT: 'Click on a field to shoot. Guess where your enemy hid their ships.',
    LOST: 'All our ships are gone. The other player has won!',
    SHIP_CARRIER: 'carrier',
    SHIP_BATTLESHIP: 'battleship',
    SHIP_DESTROYER: 'destroyer',
    SHIP_SUBMARINE: 'submarine',
    SHIP_PATROL_BOAT: 'patrol boat',
    WAIT_FOR_OPPONENT: 'Wait for %OPPONENT% to make their turn.',
    WON: 'You won!',
};