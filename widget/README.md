# Battleship Widget

Maturity: Non-compliant prototype

This widget will allows Matrix users to play Battleship with other members in a room.
As a widget it does not require the installation of a special client.

## Add to Element

Once served, use the following command to add the widget to a room:

```
/addwidget http://127.0.0.1:8080/index-widget.html#/?widgetId=$matrix_widget_id&userId=$matrix_user_id
```

## Development

The code uses example code from:
* https://github.com/turt2live/matrix-demos-nov-2020/
* https://github.com/matrix-org/matrix-widget-api/

This Widget uses [MSC 2762](https://github.com/matrix-org/matrix-doc/blob/travis/msc/widgets-send-receive-events/proposals/2762-widget-event-receiving.md) features to send and receive Matrix events.
