rm -Rf dist
mkdir -p dist/node_modules/@webcomponents/webcomponentsjs/
mkdir -p dist/node_modules/matrix-widget-api/dist/
mkdir -p dist/{locales,src}
cp node_modules/@webcomponents/webcomponentsjs/webcomponents-loader.js dist/node_modules/@webcomponents/webcomponentsjs/
cp node_modules/matrix-widget-api/dist/api.js dist/node_modules/matrix-widget-api/dist/
cp locales/* dist/locales/
cp src/* dist/src/
cp *.html dist/
ipfs add -r dist/
